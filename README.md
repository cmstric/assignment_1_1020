# README #

This program will manipulate ppm images to flip the and such. Just use the makefile to compile the programs

### Problems encountered & solutions###
* 1) A problem I encountered was in flipping the image, I originally forgot to
*    add a -1 to the for statement and also forgot to make the for statement 
*    read <= 0 since we need the first entry, it was simple to fix
* 2) Trying to figure out how the columns and rows change when flipping 90
*	  degrees left was an issue. I had to write out some numbers to represent
* 	  pixels by hand to better visualize it. Originially I was adding I to the
*	  height of the original column and that was trying to access memory out of
*	  bounds, I realized I needed to subtract by i, and as always, subtract by
* 	  1 to access the correct index of the array.
* 3) The vertical mirror threw me for a loop for a little bit, I kept getting
*	  the second half of the mirror to be black. I fixed this by initalizing
*	  my counting variable inside of the first for loop and not outside of it.

### Thoughts on the assignment ###
*	I really enjoyed this assignment, it is very rewarding to see immediate
*	results. What made this assinment so interesting to me is the fact that
*	I never truly realized how powerful C could be. Until this point we were
*	really only dealing with command window I/O and, while this is still from
*	the command window, it is awesome to see an image actually transform due to
*	code that I got to write and implement. Very good assignment that I believe
*	should be redone for later classes.
