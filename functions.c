/**************************
 * Carl Strickland
 * CPSC 1020 section 001, SP 2017
 * cmstric
 * Assignment1
 * This is the functions file for assignment 1 and will be where the fucntions
 * are made.
 ************************/

#include "functions.h"

// This will read in the header information for an image
void readHeader(header_t* hdr, FILE* fp){
   fscanf(fp, "%s %d %d %d ", hdr->magic, &hdr->width, \
   &hdr->height, &hdr->maxval);
   return;
}

// This will read in the pixel data for an image
void readImage(header_t* hdr, pixel_t** pix, FILE* fp){
   int i, j; //need to declare variables for the for loops

   for (i = 0; i < hdr->height; i++) {
      for (j = 0; j < hdr->width; j++) {
         fscanf(fp, "%c%c%c", &pix[i][j].r, &pix[i][j].g, &pix[i][j].b);
         // This is used to read in the 3 values used in the rgb values for an
         // image
      }
   }
   return;
}

// This function is a larger function which will give the user a menu which they
// can choose which transformation they wish to do
void chooseTransform(header_t* hdr, pixel_t** pix, FILE* fp){
   // Declare variable for the while and for loops
   // int i = 0; // May be unused for now so commented out
   // int j = 0; // May be uneeded so commented out for now
   int userChoice = 1;
   // Header of the function
   fprintf(stdout, "***************************"
                   "*********************************\n");
   fprintf(stdout, "Thank you for using the Image Transformer!\n");
   fprintf(stdout, "There are several transformations you can perform\n");
   fprintf(stdout, "on the input image. Choose the number");
   fprintf(stdout, " that corresponds to the\ntransformation you wish to");
   fprintf(stdout, "perform on the image!\n");
   fprintf(stdout, "***************************"
                   "*********************************\n");

   while (userChoice > 0 && userChoice < 9) {
      // Choices which the user can choose
      fprintf(stdout, "\n\n");
      fprintf(stdout, "1.  Gray Scale\n");
      fprintf(stdout, "2.  Color to Negative\n");
      fprintf(stdout, "3.  Flip the Image\n");
      fprintf(stdout, "4.  Rotate Right\n");
      fprintf(stdout, "5.  Rotate Left\n");
      fprintf(stdout, "6.  Reprint\n");
		fprintf(stdout, "7.  Mirror Vertical\n");
		fprintf(stdout, "8.  Mirror Horizontal\n");

      // Scan in what the user's answer is
      fscanf(stdin, "%d", &userChoice);
      if (userChoice == 1) {
         grayScaleImage(hdr, pix, fp);
			break;
      }
      else if (userChoice == 2) {
         color2Negative(hdr, pix, fp);
			break;
      }
      else if (userChoice == 3){
         flipImage(hdr, pix, fp);
			break;
      }
      else if (userChoice == 4){
         rotateRight(hdr, pix, fp);
			break;
      }
      else if (userChoice == 5){
         rotateLeft(hdr, pix, fp);
			break;
      }
      else if (userChoice == 6){
         reprint(hdr, pix, fp);
			break;
      }
		else if (userChoice == 7){
			mirrorVertical(hdr, pix, fp);
			break;
		}
		else if (userChoice == 8){
			mirrorHorizontal(hdr, pix, fp);
			break;
		}
      else{
         fprintf(stdout, "***************************************************");
         fprintf(stdout, "\nYou entered an incorrect number Please choose");
         fprintf(stdout, " the\nnumber that corresponds to the");
         fprintf(stdout, " transformation you\nwish to perform");
         fprintf(stdout, " on the image!\n");
         fprintf(stdout, "***************************************************");
         userChoice = 1;
      }
   }
   return;
}

void printP6Image(header_t* hdr, pixel_t** pix, FILE* fp){
   int i, j;
   // Print out the header
   fprintf(fp, "%s\n%d %d %d\n", hdr->magic, hdr->width, hdr->height, \
    hdr->maxval);
   // Print out the pixels
   for(i = 0; i < hdr->height; i++){
     for(j = 0; j < hdr->width; j++){
        fprintf(fp, "%c%c%c", pix[i][j].r, pix[i][j].g, pix[i][j].b);
     }
   }
   return;
}

void grayScaleImage(header_t * hdr, pixel_t** pix, FILE* fp){
   int i, j;
	double temp;
	fprintf(fp, "P5\n%d %d %d\n", hdr->width, hdr->height, \
	 hdr->maxval);
	unsigned char **gray = (unsigned char **)malloc(hdr-> height *\
								  sizeof(unsigned char*));
	for(i = 0; i < hdr->height; i++){
		gray[i] = (unsigned char*)malloc(hdr->width * sizeof(unsigned char));
	}
	for(i = 0; i < hdr-> height; i++){
		for(j = 0; j < hdr-> width; j++){
			temp = (pix[i][j].r*.299)+(pix[i][j].g*.587)+(pix[i][j].b*.114);
			gray[i][j] = (unsigned char)temp;
			fprintf(fp, "%c", gray[i][j]);
		}
	}
	// Free the space used in malloc
	for(i = 0; i < hdr->height; i++){
		free(gray[i]);
	}
	return;
}

void flipImage(header_t * hdr, pixel_t** pix, FILE* fp){
   int i, j, k;
   //Malloc space for the new array: out
   pixel_t **out = (pixel_t **)malloc(hdr->height * sizeof(pixel_t*));
   for (i = 0; i < hdr->height; i++)
      out[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
   // For loops to reverse the rows of the pixels
   k = 0;
   for (i = hdr->height-1; i >= 0; i--) {
      for(j = 0; j < hdr->width; j++){
         out[k][j] = pix[i][j];
      }
      k++;
   }
   printP6Image(hdr, out, fp);
	// Free the space used in malloc
	for(i = 0; i < hdr->height; i++){
		free(out[i]);
	}
   return;
}

void rotateLeft(header_t * hdr, pixel_t** pix, FILE* fp){
	int i, j, temp;
	int k = 0;
	// Switch the height and width of the picture in the header
	temp = hdr->height;
	hdr->height = hdr->width;
	hdr->width = temp;
	// Malloc space for the new array to be made
	pixel_t **new = (pixel_t**)malloc(hdr->height * sizeof(pixel_t*));
	for (i = 0; i < hdr->height; i++){
		new[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
	}
	for(i = 0; i < hdr->height; i++){
		for(j = 0; j < hdr->width; j++){
			new[i][j] = pix[j][hdr->height-i-1 ];
		}
	}
	printP6Image(hdr, new, fp);
	// Free the space used in malloc
	for(i = 0; i < hdr->height; i++){
		free(new[i]);
	}
   return;
}

void rotateRight(header_t * hdr, pixel_t** pix, FILE* fp){
	int i, j, temp;
	temp = hdr->height;
	hdr->height = hdr->width;
	hdr->width = temp;
	// Malloc space for the new array to be made
	pixel_t **new = (pixel_t**)malloc(hdr->height * sizeof(pixel_t*));
	for (i = 0; i < hdr->height; i++){
		new[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
	}
	for(i = 0; i < hdr-> height; i++){
		for(j = 0; j < hdr-> width; j++){
			new[i][j] = pix[hdr->width - j - 1][i];
		}
	}
	printP6Image(hdr, new, fp);
	// Free the space used in malloc
	for(i = 0; i < hdr->height; i++){
		free(new[i]);
	}
   return;
}

void color2Negative(header_t* hdr, pixel_t** pix, FILE* fp){
	int i, j;
	double temp;
	// malloc space for negative array
	pixel_t **neg = (pixel_t**)malloc(hdr->height * sizeof(pixel_t*));
	for (i = 0; i < hdr->height; i++){
		neg[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
	}
	for(i = 0; i < hdr-> height; i++){
		for(j = 0; j < hdr-> width; j++){
			// Loop through each pixel value and change them to negative
			temp = 255 - pix[i][j].r;
			neg[i][j].r = (unsigned char)temp;
			temp = 255 - pix[i][j].g;
			neg[i][j].g = (unsigned char)temp;
			temp = 255 - pix[i][j].b;
			neg[i][j].b = (unsigned char)temp;
		}
	}
	printP6Image(hdr, neg, fp);
	// Free the space used in malloc
	for(i = 0; i < hdr->height; i++){
		free(neg[i]);
	}
   return;
}

void reprint(header_t* hdr, pixel_t** pix, FILE* fp){
   printP6Image(hdr, pix, fp);
   return;
}

void mirrorVertical(header_t* hdr, pixel_t** pix, FILE* fp){
   int i, j;
	int k;
	int odd = 0;
	//Malloc space for the new array: out
   pixel_t **out = (pixel_t **)malloc(hdr->height * sizeof(pixel_t*));
   for (i = 0; i < hdr->height; i++)
      out[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
	// Checking if width is even or odd, if odd add 1 in the for loop.
	if((hdr->width) % 2 != 0){
		odd = 1;
	}
	// For loops to go halfway through the image and mirror it
	for(i = 0; i < hdr-> height; i++){
		k = 0;  								// Initalize k inside for loop
		for(j = 0; j < hdr-> width; j++){
			if(j < (hdr -> width/2) + odd){
				out[i][j] = pix[i][j];
			}
			// else statement to check the condition of the width
			else{
				out[i][j] = pix[i][((hdr-> width/2) + odd) - k];
				k++;
			}
		}
	}
	printP6Image(hdr, out, fp);
	// Free space used for the array
	for(i = 0; i < hdr-> height; i++){
		free(out[i]);
	}
	return;
}

void mirrorHorizontal(header_t* hdr, pixel_t** pix, FILE* fp){
   int i, j, k;
	int odd = 0;
	// double the size of the height
	if((hdr-> height)%2 != 0){
		odd = 1;
	}
	hdr-> height = (hdr-> height) * 2;
	//Malloc space for the new array: out
   pixel_t **out = (pixel_t **)malloc(hdr->height * sizeof(pixel_t*));
   for (i = 0; i < hdr->height; i++)
      out[i] = (pixel_t*)malloc(hdr->width * sizeof(pixel_t));
	
	// For loop to print the original picture
   for(k = 0; k < hdr->height/2 + odd; k++){
     for(j = 0; j < hdr->width; j++){
		  out[k][j] = pix[k][j];
     }
   }
	// For loop to flip the image and print
   for (i = ((hdr-> height)/2)-1 + odd; i >= 0; i--) {
      for(j = 0; j < hdr->width; j++){
         out[k][j] = pix[i][j];
      }
      k++;
   }
	printP6Image(hdr, out, fp);
	// Free space used for the array
	for(i = 0; i < hdr-> height; i++){
		free(out[i]);
	}
	
	return;
}
