/*****************************************
 * Carl Strickland
 * CPSC 1020 Section 001, Spring 2017
 * cmstric
 * Assignment 1
 *
 * This program is the header file that is going to be use for the driver and
 * functions program.
 ****************************************/

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <stdio.h>
#include <stdlib.h>

typedef struct{
   char magic[3];
   int height, width, maxval;
}header_t;

typedef struct{
   unsigned char r, g, b;
}pixel_t;

void readHeader(header_t*, FILE*);
void readImage(header_t*, pixel_t **, FILE*);
void chooseTransform(header_t *, pixel_t **, FILE*);
void printP6Image(header_t*, pixel_t **, FILE*);
void grayScaleImage(header_t *, pixel_t **, FILE*);
void flipImage(header_t *, pixel_t **, FILE*);
void rotateLeft(header_t * , pixel_t **, FILE*);
void rotateRight(header_t *, pixel_t **, FILE*);
void color2Negative(header_t*, pixel_t **, FILE*);
void reprint(header_t*, pixel_t**, FILE*);
void mirrorVertical(header_t*, pixel_t**, FILE*);
void mirrorHorizontal(header_t*, pixel_t**, FILE*);

#endif
