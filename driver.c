/**************************
 * Carl Strickland
 * CPSC 1020 section 001, SP 2017
 * cmstric
 * Assignment1
 * This is the driver function which will implement the functions.c
 * and will contain all the order for the file
 ************************/

#include "functions.h"

int main(int argc, char* argv[])
{
   FILE* input = fopen(argv[1], "r");
   FILE* output = fopen(argv[2], "w");
   int i;
   // Check number of command-line arguments
   if (argc < 3) {
      // argc should be 2 for correct execution
      printf("usage: %s input_file output_file\n", argv[0]);
      return 1;
   }
   else{
      // Check to make sure the file opened successfully
      if (input == NULL) {
         printf("Can't open %s.\n", argv[1]);
         return 2;
      }
      if (output == NULL){
         printf("Can't open %s.\n", argv[2]);
         return 3;
      }
   } // end else
   header_t hdr;
   readHeader(&hdr, input); // Reading the header to get the values Wid/Length
   // Dynamically allocate the memory for the Image and make the array for pix
   pixel_t **image = (pixel_t **)malloc(hdr.height * sizeof(pixel_t*));
   for (i = 0; i < hdr.height; i++)
      image[i] = (pixel_t*)malloc(hdr.width * sizeof(pixel_t));
   // read the Image in and store it in the array for the pixels
   readImage(&hdr, image, input);
   // The user now chooses the transform they wish to use
   chooseTransform(&hdr, image, output);
   free(image); // Free the malloced space
   fclose(input);
   fclose(output);
   return 0;
}
